pragma solidity 0.5.8;

contract Subscription {
    // Read/write foodChoice
    string public foodChoice;

    // Constructor
    constructor() public {
        foodChoice = "Food Choice 1";
    }
}
